module gitlab.com/constraintAutomaton/NonaCard

go 1.14

require (
	github.com/GeertJohan/go.rice v1.0.0
	github.com/gorilla/mux v1.7.4
	github.com/joho/godotenv v1.3.0
)
