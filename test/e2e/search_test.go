package e2e

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"reflect"
	"testing"

	response "gitlab.com/constraintAutomaton/NonaCard/domain/animeRepository/response"

	"gitlab.com/constraintAutomaton/NonaCard/server"
)

var ROOT_SERVER_ADDR string = fmt.Sprintf("http://localhost:%s", server.DEFAULT_PORT)

const ANIME_SEARCH_ROUTE string = "api/v1/search/anime"

var SEARCH_QUERY = []string{"berserk",
	"harry",
	"",
	")",
	"."}

// we test that the request can be done and is not empty. we don't test the validity
// of the result because they come from an external API
func TestAValidAnimeSearchQuery(t *testing.T) {
	srv := &http.Server{}
	close := make(chan os.Signal, 1)
	go server.BuildServer(srv, close)
	defer server.SendCloseSignal(close)

	for _, query := range SEARCH_QUERY {
		resp, err := makeAnAnimeSearchResearch(query)

		if err != nil {
			t.Errorf(err.Error())
		}
		if resp.StatusCode != http.StatusOK {
			t.Errorf("request should be ok the code is : %s", resp.Status)
		}
		if testIfResponseIsValid(resp) != nil {
			t.Errorf(err.Error())
		}

	}

}

func makeAnAnimeSearchResearch(query string) (*http.Response, error) {
	return http.Get(fmt.Sprintf("%s/%s?q=%s",
		ROOT_SERVER_ADDR,
		ANIME_SEARCH_ROUTE,
		query))
}

func testIfResponseIsValid(resp *http.Response) error {
	defer resp.Body.Close()

	searchAnimeResponse := response.SearchResponse{}
	var mapResponse map[string]interface{}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	errTransfertToStruct := json.Unmarshal(body, &searchAnimeResponse)
	errTransfertToMap := json.Unmarshal(body, &mapResponse)

	if errTransfertToStruct != nil || errTransfertToMap != nil {
		return err
	}
	if reflect.DeepEqual(searchAnimeResponse, response.SearchResponse{}) {
		if len(mapResponse) != 0 {
			return fmt.Errorf("the data sended don't respect the interface")
		}
	}

	return nil
}
