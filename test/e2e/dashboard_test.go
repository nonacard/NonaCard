package e2e

import (
	"fmt"
	"net/http"
	"os"
	"testing"

	"gitlab.com/constraintAutomaton/NonaCard/server"
)

const DASHBOARD_ROUTE string = "api/v1/search/anime"

func TestARequestToTheDashboard(t *testing.T) {
	srv := &http.Server{}
	close := make(chan os.Signal, 1)
	go server.BuildServer(srv, close)
	defer server.SendCloseSignal(close)

	resp, err := queryTheDashboard()
	if err != nil {
		t.Errorf(err.Error())
	}
	if errInvalidRequest := testIfDashboardRouteValid(resp); errInvalidRequest != nil {
		t.Errorf(err.Error())
	}

}

func queryTheDashboard() (*http.Response, error) {
	return http.Get(fmt.Sprintf("%s/%s",
		ROOT_SERVER_ADDR,
		DASHBOARD_ROUTE))
}

func testIfDashboardRouteValid(resp *http.Response) error {
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("request should be ok the code is : %s", resp.Status)
	}
	return nil
}
