package animeRepository

import (
	"encoding/json"
	"io/ioutil"
	"reflect"
	"testing"

	animerepository "gitlab.com/constraintAutomaton/NonaCard/domain/animeRepository"
	"gitlab.com/constraintAutomaton/NonaCard/domain/animeRepository/response"
	mock "gitlab.com/constraintAutomaton/NonaCard/mock/remote-communication"
)

const A_VALID_REQUEST string = "valid"
const INVALID_REQUEST string = "invalid"
const SAMPLE_RESPONSE_JSON string = "./sampleResponse.json"
const EXPECTED_RESPONSE_JSON string = "./expectedResponse.json"

func TestAValidRequest(t *testing.T) {
	anilist := provideAnilist()

	expectedResp, _ := provideExpectedResponse()
	resp, err := anilist.SearchByName(A_VALID_REQUEST)
	if err != nil {
		t.Errorf(err.Error())
	} else if !reflect.DeepEqual(resp, expectedResp) {
		t.Errorf("resp is not equal to the expected response")
	}
}

func TestAnInvalidRequest(t *testing.T) {
	anilist := provideAnilist()
	_, err := anilist.SearchByName(INVALID_REQUEST)
	if err == nil {
		t.Errorf(err.Error())
	}
}

func provideAnilist() animerepository.Anilist {
	remoteCommunication := mock.MockRemoteCommunication{File: SAMPLE_RESPONSE_JSON}
	return animerepository.Anilist{RemoteCommunicationModule: remoteCommunication}
}

func provideExpectedResponse() (response.SearchResponse, error) {
	var resp response.SearchResponse
	m, err := ioutil.ReadFile(EXPECTED_RESPONSE_JSON)
	if err != nil {
		return response.SearchResponse{}, err
	}
	err = json.Unmarshal(m, &resp)
	if err != nil {
		return response.SearchResponse{}, err
	}
	return resp, nil
}
