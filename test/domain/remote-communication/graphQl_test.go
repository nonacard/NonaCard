package remoteCommunication

import (
	"net/http"
	"net/http/httptest"
	"testing"

	graphQl "gitlab.com/constraintAutomaton/NonaCard/domain/remote-communication"
	mock "gitlab.com/constraintAutomaton/NonaCard/mock/remote-communication"
)

const SAMPLE_RESPONSE_JSON string = "./sampleResponse.json"

func TestAValidRequest(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(mock.GraphQlValidResponse(SAMPLE_RESPONSE_JSON)))
	defer server.Close()
	client := provideGraphQlClient(server.Client())
	err := client.Fetch(provideParameterQuery(server.URL))

	if err != nil {
		t.Errorf(err.Error())
	}
}

func TestInvalidRequest(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(mock.InvalidResponse()))
	defer server.Close()
	client := provideGraphQlClient(server.Client())
	err := client.Fetch(provideParameterQuery(server.URL))
	if err == nil {
		t.Errorf("This request should return an error")
	}
}

func provideGraphQlClient(client *http.Client) graphQl.GraphQlClient {
	return graphQl.GraphQlClient{Client: client}
}

func provideParameterQuery(url string) graphQl.ParameterQuery {
	return graphQl.ParameterQuery{
		Url:           url,
		Query:         "",
		Variables:     new(map[string]string),
		Out:           new(map[string]interface{}),
		Authorization: make([]string, 0),
	}
}
