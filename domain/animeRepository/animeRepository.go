package animerepository

import response "gitlab.com/constraintAutomaton/NonaCard/domain/animeRepository/response"

// AnimeRepository interface of an anime repository
type AnimeRepository interface {
	SearchByName(query string) (response.SearchResponse, error)
}

// An unknown internal error
const InternalError string = "internal error"
