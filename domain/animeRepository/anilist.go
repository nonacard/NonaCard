package animerepository

import (
	"errors"

	response "gitlab.com/constraintAutomaton/NonaCard/domain/animeRepository/response"
	remoteCommunication "gitlab.com/constraintAutomaton/NonaCard/domain/remote-communication"
)

const urlAnilist string = "https://graphql.anilist.co"

// Anilist module of the anilist database API at https://graphql.anilist.co
type Anilist struct {
	RemoteCommunicationModule remoteCommunication.RemoteCommuncation
}

// SearchByName return the result of a search by name in the anilist database
func (anilist Anilist) SearchByName(q string) (response.SearchResponse, error) {
	variable := map[string]string{
		"name": q}
	out := response.SearchAnilistJSON{}
	query := remoteCommunication.ParameterQuery{
		Url:           urlAnilist,
		Query:         querySearchAnilist,
		Variables:     &variable,
		Out:           &out,
		Authorization: make([]string, 0)}

	err := anilist.RemoteCommunicationModule.Fetch(query)
	if err != nil {
		return response.SearchResponse{}, err
	}
	if searchAnilistJSON, ok := query.Out.(*response.SearchAnilistJSON); ok {
		return anilist.formatSearchResponse(searchAnilistJSON), nil
	} else {
		return response.SearchResponse{}, errors.New(InternalError)
	}
}

// formatSearchResponse formate the into the response interface
func (anilist Anilist) formatSearchResponse(res *response.SearchAnilistJSON) response.SearchResponse {
	resFormated := response.SearchResponse{Result: res.Data.Page.Media}
	return resFormated
}

const querySearchAnilist = `query ($name: String) {
	Page {
	  media(search: $name, type: ANIME) {
		id
		idMal
		averageScore
		siteUrl
		title {
		  romaji
		  english
		}
		coverImage {
		  medium
		  large
		  color
		}
		description
		tags{
			description
			name
		  }
	  }
	}
  }`
