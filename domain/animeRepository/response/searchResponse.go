package response

// SearchResponse response of a search in an anime repository
type SearchResponse struct {
	Result []result `json:"data"`
}

type result struct {
	ID           int        `json:"id"`
	IDMal        int        `json:"idMal"`
	AverageScore float64    `json:"averageScore"`
	SiteURL      string     `json:"siteUrl"`
	Title        title      `json:"title"`
	CoverImage   coverImage `json:"coverImage"`
	Description  string     `json:"description"`
	Tags         []Tags     `json:"tags"`
}
type title struct {
	Romaji  string `json:"romaji"`
	English string `json:"english"`
}
type coverImage struct {
	Medium string `json:"medium"`
	Large  string `json:"large"`
	Color  string `json:"color"`
}

type Tags struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}
