package remotecommuncation

// RemotreCommuncation interface of module that communicate to remotes sources
type RemoteCommuncation interface {
	Fetch(query ParameterQuery) error
}

// ParameterQuery parameter of a remote communication query data is send to the out parameter
type ParameterQuery struct {
	Url           string
	Query         string
	Variables     *map[string]string
	Out           interface{}
	Authorization []string
}

const ApiError string = "API send http error %d"
