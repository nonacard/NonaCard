package server

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	route "gitlab.com/constraintAutomaton/NonaCard/rest/route"
)

const DEFAULT_PORT string = "8080"

func setRoute(r *mux.Router) {

	api := r.PathPrefix("/api/v1").Subrouter()

	r.HandleFunc("/", route.MainPage).Methods("GET")
	r.HandleFunc("/login", route.Login).Methods("GET")

	api.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html; charset=UTF-8")
		fmt.Fprintf(w, "<h1>Welcome the API</h1>")
	}).Methods("GET")

	searchAnime := api.PathPrefix("/search/anime").Subrouter()
	searchAnime.HandleFunc("", route.SearchAnime).Methods("GET")
}

func initializeServer() *mux.Router {
	r := mux.NewRouter()
	r.Use(mux.CORSMethodMiddleware(r))

	static := r.PathPrefix("/static/")
	fs := http.FileServer(http.Dir("assets/"))

	static.Handler(http.StripPrefix("/static/", fs))
	return r
}

func getPort() string {
	port := os.Getenv("PORT")
	if port == "" {
		port = DEFAULT_PORT
		log.Println("[-] No PORT environment variable detected. Setting to ", port)
	} else {
		log.Println("Starting app at ", port)
	}
	return ":" + port
}

// BuildServer build a server, provide the route to it and link the close signal to the server
func BuildServer(srv *http.Server, close chan os.Signal) {
	if err := godotenv.Load(".env"); err != nil {
		log.Println("No .env file found")
	}

	r := initializeServer()
	setRoute(r)

	srv.Addr = getPort()
	srv.Handler = r

	signal.Notify(close, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	<-close
	closeServer(srv)
}

// send the a close server signal
func SendCloseSignal(close chan os.Signal) {
	close <- os.Interrupt
}

func closeServer(srv *http.Server) {
	log.Print("Server Stopped")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Server Shutdown Failed:%+v", err)
	}
	log.Print("Server Exited Properly")
}
