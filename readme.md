# NonaCard

An anime 3X3 builder linked with the anilist API

https://nonacard.herokuapp.com

# support 
linux

## Building
You have to set a `.env` file following the template of the `.env_example`. To get Anilist credential you have to follow this guide https://anilist.gitbook.io/anilist-apiv2-docs/overview/oauth/authorization-code-grant

after this done, enter this command
```
make build
```
then run the executable in the root directory

