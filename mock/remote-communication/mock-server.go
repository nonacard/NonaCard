package remoteCommunication

import (
	"io/ioutil"
	"net/http"
)

func GraphQlValidResponse(response string) func(rw http.ResponseWriter, req *http.Request) {
	return func(rw http.ResponseWriter, req *http.Request) {
		m, err := ioutil.ReadFile(response)

		if err != nil {
			_, _ = rw.Write([]byte("file could not be read"))
		}

		_, err = rw.Write(m)

		if err != nil {
			_, _ = rw.Write([]byte("unable to write into the response"))
		}
	}
}

func InvalidResponse() func(rw http.ResponseWriter, req *http.Request) {
	return func(rw http.ResponseWriter, req *http.Request) {
		http.Error(rw, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	}
}
