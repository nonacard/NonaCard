package remoteCommunication

import (
	"encoding/json"
	"errors"
	"io/ioutil"

	remotecommuncation "gitlab.com/constraintAutomaton/NonaCard/domain/remote-communication"
)

type MockRemoteCommunication struct {
	File string
}

func (mock MockRemoteCommunication) Fetch(param remotecommuncation.ParameterQuery) error {
	m, err := ioutil.ReadFile(mock.File)
	variables := *param.Variables
	if variables["name"] == "invalid" {
		return errors.New("Invalid request")
	}
	if err != nil {
		return err
	}
	err = json.Unmarshal(m, param.Out)
	if err != nil {
		return err
	}
	return nil
}
