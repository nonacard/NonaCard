package route

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	animerepository "gitlab.com/constraintAutomaton/NonaCard/domain/animeRepository"
	response "gitlab.com/constraintAutomaton/NonaCard/domain/animeRepository/response"
	remotecommuncation "gitlab.com/constraintAutomaton/NonaCard/domain/remote-communication"
)

// SearchAnime search anime info
func SearchAnime(w http.ResponseWriter, r *http.Request) {
	animeName := r.FormValue("q")
	remoteCommuntication := remotecommuncation.MakeGraphQlClient()
	anilistRepo := animerepository.Anilist{RemoteCommunicationModule: remoteCommuntication}
	resp, err := anilistRepo.SearchByName(animeName)
	if err != nil {
		sendInternalError(&w, err)
	} else {
		sendResponse(&resp, &w)
	}
}
func sendResponse(resp *response.SearchResponse, w *http.ResponseWriter) {
	b, err := json.Marshal(*resp)
	if err != nil {
		sendInternalError(w, err)
	} else {
		_, err = io.WriteString(*w, string(b))
		if err != nil {
			sendInternalError(w, err)
		}
		(*w).Header().Set("Content-Type", "text/json; application/json")
	}
}
func sendInternalError(w *http.ResponseWriter, err error) {
	log.Println(err)
	http.Error(*w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	fmt.Fprintf(*w, "")
}
