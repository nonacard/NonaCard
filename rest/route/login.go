package route

import (
	"log"
	"net/http"

	"gitlab.com/constraintAutomaton/NonaCard/rest/login"
)

// Login log an user
func Login(w http.ResponseWriter, r *http.Request) {
	token := r.URL.Query()["code"][0]
	logger := login.NewAnilistLogger(token)

	jwt, err := logger.GetJwt()
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	} else {
		log.Println(jwt)
	}
	http.Redirect(w, r, "", http.StatusSeeOther)
}
