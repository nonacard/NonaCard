package route

import (
	"fmt"
	"html/template"
	"net/http"
	"os"

	rice "github.com/GeertJohan/go.rice"
	private "gitlab.com/constraintAutomaton/NonaCard/rest/viewsInterface"
)

// MainPage generate the NonaCard main page
func MainPage(w http.ResponseWriter, r *http.Request) {

	aouthLink := fmt.Sprintf(os.Getenv("OAUTH_LINK"), os.Getenv("CLIENT_ID"), os.Getenv("REDIRECT_URL"))
	cardIdentifier := []string{"card-1", "card-2", "card-3", "card-4", "card-5", "card-6", "card-7", "card-8", "card-9"}
	data := pageSetup{
		CardIds: cardIdentifier,
		Header: private.Header{
			CSS:   "static/css/main.css",
			Title: "NonaCard"},
		Footer: private.Footer{
			Js: "/static/dist/main.js"},
		HeaderDashboard: private.HeaderDashboard{
			OauthLink: aouthLink}}
	dashboardTemplate, errTemplate := getTemplate()

	errExecute := dashboardTemplate.Execute(w, data)
	if errExecute != nil || errTemplate != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	}
}
func getTemplate() (*template.Template, error) {
	viewsBox, err := rice.FindBox("../views")
	files := []string{"main.html",
		"components/header.html",
		"components/footer.html",
		"components/dashboard/card.html",
		"components/dashboard/footer.html",
		"components/dashboard/header.html"}
	tmpl := template.New("dashboard")
	if err != nil {
		return tmpl, err
	}
	for _, el := range files {
		fileContent, err := viewsBox.String(el)
		if err != nil {
			return tmpl, err
		}
		tmpl, err = tmpl.Parse(fileContent)
		if err != nil {
			return tmpl, err
		}
	}
	return tmpl, nil
}

type pageSetup struct {
	Header          private.Header
	Footer          private.Footer
	HeaderDashboard private.HeaderDashboard
	CardIds         []string
}
