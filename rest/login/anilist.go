package login

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
)

type anilist struct {
	oauthInfo *OauthInfo
	name      string
	url       string
}

const OAUTH_LINK string = "https://anilist.co/api/v2/oauth/token"

// provide an anilist login user
func NewAnilistLogger(token string) *anilist {
	return &anilist{oauthInfo: ProvideOauthAnilist(token), name: "anilist", url: OAUTH_LINK}
}

// provide an OauthInfo struct
func ProvideOauthAnilist(token string) *OauthInfo {
	return &OauthInfo{GrantType: "authorization_code",
		ClientID:     os.Getenv("CLIENT_ID"),
		ClientSecret: os.Getenv("CLIENT_SECRET"),
		RedirectURI:  os.Getenv("REDIRECT_URL"),
		Code:         token,
	}
}

func (a anilist) Name() string {
	return a.name
}

func (a anilist) Url() string {
	return a.url
}

// provide a valid jwt
func (a anilist) GetJwt() (string, error) {
	infoJSON, err := json.Marshal(a.oauthInfo)
	if err != nil {
		return "", err
	}
	resp, err := http.Post(a.url, "application/json", bytes.NewBuffer(infoJSON))
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	return string(body), nil
}
