package login

// Login interface to a module to log an user
type Login interface {
	Name() string
	Url() string
	GetJwt() (string, error)
}

// OauthInfo info to log a user via a Oauth
type OauthInfo struct {
	GrantType    string `json:"grant_type"`
	ClientID     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	RedirectURI  string `json:"redirect_uri"`
	Code         string `json:"code"`
}
