package main

import (
	"net/http"
	"os"

	"gitlab.com/constraintAutomaton/NonaCard/server"
)

func main() {
	srv := &http.Server{}
	close := make(chan os.Signal, 1)
	server.BuildServer(srv, close)
}
